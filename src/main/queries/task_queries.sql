/*1. Показать всех должников (чей товар доставлен, а сумма по договору не совпадает с суммой по платежам) по договорам с сортировкой по максимальному долгу*/

select name
from (select
        amount as amount_contract,
        id_contract
      from (select t2.id_contract
            from (select
                    count(id_product) as count_product,
                    t1.id_contract
                  from (select
                          shipment.id as id_shipment,
                          shipment.id_contract,
                          c2.amount   as contract_amount,
                          id_organization
                        from shipment
                          full join contract c2 on shipment.id_contract = c2.id) as t1 full join tt_product_shipment
                      on t1.id_shipment = tt_product_shipment.id_shipment
                  group by t1.id_contract) as t2, (select
                                                     count(id_product) as count_product,
                                                     id_contract
                                                   from contract
                                                     full join tt_product_contract c3 on contract.id = c3.id_contract
                                                   group by id_contract) as t3
            where t2.id_contract = t3.id_contract and t2.count_product = t3.count_product) as t4
        join contract on id_contract = contract.id) as pre1,
  (select
     id_organization,
     (amount - sum_payment) as remains,
     id_contract
   from contract
     full join (select
                  sum(amount) as sum_payment,
                  id_contract
                from payment
                group by id_contract) as pt on contract.id = id_contract
   where sum_payment < amount) as pre2, organization
where pre1.id_contract = pre2.id_contract and organization.id = pre2.id_organization
order by pre2.remains desc /*сортировка :)*/;

/*2. Показать самых лучших клиентов (принесших большую прибыль)*/

select
  pre1.sum_amount,
  organization.name
from
  (select
     am1.sum_amount,
     contract.id_organization
   from (
          select
            id_contract,
            sum(amount) as sum_amount
          from payment
          group by id_contract) as am1
     join contract on am1.id_contract = contract.id) as pre1
  join organization on pre1.id_organization = organization.id
order by pre1.sum_amount desc
limit 3;

/*3. Показать самый популярный продукт доставки и количество за месяц*/
select
  product.name as prod_name,
  count_shipment
from
  (select
     count_shipment,
     id_product
   from
     (select
        id_product,
        count(id_shipment) as count_shipment
      from tt_product_shipment
        join product p on tt_product_shipment.id_product = p.id
        join shipment s2 on tt_product_shipment.id_shipment = s2.id
      where delivery_date > now() - interval '30 day'
      group by id_product) as t1
   where count_shipment = (select max(zb.count_shipment)
                           from (select count(id_shipment) as count_shipment
                                 from tt_product_shipment
                                   join product p on tt_product_shipment.id_product = p.id
                                   join shipment s2 on tt_product_shipment.id_shipment = s2.id
                                 where delivery_date > now() - interval '30 day'
                                 group by id_product) as zb)) as pre1
  join product on pre1.id_product = product.id;

/*для этого запроса, в настоящий момент, из-за маленького количества наполнения, везде выдаст по одному товару.
Нарушая полноту данных, в первую попавшуюся доставку добавляем первый попавшийся товар*/
insert into tt_product_shipment (id_product, id_shipment) values ((select id
                                                                   from product
                                                                   limit 1), (select id
                                                                              from shipment
                                                                              limit 1));


/*4. Показать самый популярный город доставки*/

select
  location.city,
  count_contract as count_deliveries
from
  (select *
   from (select
           id_location,
           count(id_contract) as count_contract
         from contract
           join tt_contract_location location2 on contract.id = location2.id_contract
           join location l on location2.id_location = l.id
         group by id_location) as pre1
   where count_contract = (select max(count_contract)
                           from (select
                                   id_location,
                                   count(id_contract) as count_contract
                                 from contract
                                   join tt_contract_location location2 on contract.id = location2.id_contract
                                   join location l on location2.id_location = l.id
                                 group by id_location) as in1)) as pre2
  join location on pre2.id_location = location.id;

/*5. Показать информацию по самым долгим, в плане доставки, направлениям*/

select
  delivery_time,
  location.city
from
  (select
     shipment.id_contract,
     (delivery_date - departure_date) as delivery_time,
     tt_contract_location.id_location
   from shipment
     join tt_contract_location on shipment.id_contract = tt_contract_location.id_contract
   where (delivery_date - departure_date) = (
     select max(delivery_date - departure_date)
     from shipment)) as pre1
  join location on pre1.id_location = location.id;
