/*insert into organization (name) values ('рога и копыта'), ('architects'), ('suicide silence'), ('emmure');*/

/*insert into product (name)
values ('электро гитара'), ('акустическая гитара'), ('бас гитара'), ('барабан'), ('усилитель'), ('колонка'),
  ('микрофон'), ('наушники'), ('синтезатор');*/

/*insert into location (city, address) values ('Москва', 'ул Безымянная, дом адцатый,');*/
/*insert into location (city, address) values ('Брайтон', 'ул Длинная, дом высокий,');*/
/*insert into location (city, address) values ('Риверсайд', 'где-то в касл парке');*/
/*insert into location (city, address) values ('Куинс', 'пусть будет Лонг-Айленд Экспрессуэй');*/

/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'рога и копыта'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Москва'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'architects'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Брайтон'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'suicide silence'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Риверсайд'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'emmure'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Куинс'));*/

insert into contract (amount, details, id_organization)
values ('200', 'второй заказ', (select id
                                from organization
                                where name = 'architects'))
returning id;

insert into tt_product_contract (id_product, id_contract) values ((select id
                                                                   from product
                                                                   where name = 'акустическая гитара'),
                                                                  (select id
                                                                   from contract
                                                                   where details = 'второй заказ'));
insert into tt_product_contract (id_product, id_contract) values ((select id
                                                                   from product
                                                                   where name = 'синтезатор'),
                                                                  (select id
                                                                   from contract
                                                                   where details = 'второй заказ'));

insert into tt_contract_location (id_contract, id_location) values ((select id
                                                                     from contract
                                                                     where contract.details = 'второй заказ'),
                                                                    (select tt_organization_location.id_location
                                                                     from tt_organization_location
                                                                     where id_organization =
                                                                           (select contract.id_organization
                                                                            from contract
                                                                            where details = 'второй заказ')));


insert into payment (id_contract, type, date, amount) VALUES ((select id
                                                               from contract
                                                               where details = 'второй заказ'), 'телефонный заказ',
                                                              '2018-04-20', 100);
insert into payment (id_contract, type, date, amount) VALUES ((select id
                                                               from contract
                                                               where details = 'второй заказ'), 'телефонный заказ',
                                                              '2018-04-22', 100);

insert into shipment (delivery_date, planned_delivery_date, departure_date, planned_departure_date) values
                       ('2018-04-25',         '2018-04-23',     '2018-04-23',       '2018-04-22')
returning id;

insert into tt_product_shipment (id_product, id_shipment) values ((select id
                                                                   from product
                                                                   where product.name = 'акустическая гитара'), (select id
                                                                                                      from shipment
                                                                                                      where
                                                                                                        delivery_date =
                                                                                                        '2018-04-25' and
                                                                                                        departure_date =
                                                                                                        '2018-04-23'));
insert into tt_product_shipment (id_product, id_shipment) values ((select id
                                                                   from product
                                                                   where product.name = 'синтезатор'), (select id
                                                                                                      from shipment
                                                                                                      where
                                                                                                        delivery_date =
                                                                                                        '2018-04-25' and
                                                                                                        departure_date =
                                                                                                        '2018-04-23'));






