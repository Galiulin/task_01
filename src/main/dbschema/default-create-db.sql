create table organization
(
	id serial not null
		constraint organization_pkey
			primary key,
	name varchar(50) not null
)
;

create unique index organization_id_uindex
	on organization (id)
;

create unique index organization_name_uindex
	on organization (name)
;

create table product
(
	id serial not null
		constraint product_pkey
			primary key,
	name varchar(50) not null
)
;

create unique index product_id_uindex
	on product (id)
;

create unique index product_name_uindex
	on product (name)
;

create table location
(
	id serial not null
		constraint location_pkey
			primary key,
	city varchar(50) not null,
	address varchar(50) not null
)
;

create unique index location_id_uindex
	on location (id)
;

create table contract
(
	id serial not null
		constraint contract_pkey
			primary key,
	amount integer not null,
	details text,
	id_organization integer not null
		constraint contract_organization_id_fk
			references organization
)
;

create unique index contract_id_uindex
	on contract (id)
;

create table payment
(
	id serial not null
		constraint payment_pkey
			primary key,
	id_contract integer not null
		constraint payment_contract_id_fk
			references contract,
	type varchar(20),
	date timestamp not null,
	amount integer not null
)
;

comment on table payment is 'платеж по договору'
;

comment on column payment.type is 'тип'
;

comment on column payment.date is 'дата заключения'
;

create unique index payment_id_uindex
	on payment (id)
;

create table shipment
(
	id serial not null
		constraint shipment_pkey
			primary key,
	delivery_date timestamp,
	planned_delivery_date timestamp,
	departure_date timestamp,
	planned_departure_date timestamp
)
;

comment on table shipment is 'доставка'
;

comment on column shipment.delivery_date is 'фактическая дата доставки'
;

comment on column shipment.planned_delivery_date is 'планируемая дата доставки'
;

comment on column shipment.departure_date is 'фактическая дата отправки'
;

comment on column shipment.planned_departure_date is 'планируемая дата отправки'
;

create unique index shipment_id_uindex
	on shipment (id)
;

create table tt_product_shipment
(
	id_product integer not null
		constraint tt_product_shipment_product_id_fk
			references product,
	id_shipment integer not null
		constraint tt_product_shipment_shipment_id_fk
			references shipment
)
;

create table tt_product_contract
(
	id_product integer not null
		constraint tt_product_contract_product_id_fk
			references product,
	id_contract integer not null
		constraint tt_product_contract_contract_id_fk
			references contract
)
;

create table tt_contract_location
(
	id_contract integer not null
		constraint tt_contract_location_contract_id_fk
			references contract,
	id_location integer not null
		constraint tt_contract_location_location_id_fk
			references location
)
;

create table tt_organization_location
(
	id_organization integer not null
		constraint tt_organization_location_organization_id_fk
			references organization,
	id_location integer not null
		constraint tt_organization_location_location_id_fk
			references location
)
;

