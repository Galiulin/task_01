/*insert into organization (name) values ('рога и копыта'), ('architects'), ('suicide silence'), ('emmure');*/

/*insert into product (name)
values ('электро гитара'), ('акустическая гитара'), ('бас гитара'), ('барабан'), ('усилитель'), ('колонка'),
  ('микрофон'), ('наушники'), ('синтезатор');*/

/*insert into location (city, address) values ('Москва', 'ул Безымянная, дом адцатый,');*/
/*insert into location (city, address) values ('Брайтон', 'ул Длинная, дом высокий,');*/
/*insert into location (city, address) values ('Риверсайд', 'где-то в касл парке');*/
/*insert into location (city, address) values ('Куинс', 'пусть будет Лонг-Айленд Экспрессуэй');*/

/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'рога и копыта'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Москва'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'architects'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Брайтон'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'suicide silence'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Риверсайд'));*/
/*insert into tt_organization_location (id_organization, id_location) values ((select id
                                                                             from organization
                                                                             where name = 'emmure'),
                                                                            (select id
                                                                             from location
                                                                             where city = 'Куинс'));*/

insert into contract (amount, details, id_organization)
values ('300', 'четвертый_заказ', (select id
                                from organization
                                where name = 'suicide silence'))
returning id;

insert into tt_product_contract (id_product, id_contract) values ((select id
                                                                   from product
                                                                   where name = 'усилитель'),
                                                                  (select id
                                                                   from contract
                                                                   where details = 'четвертый_заказ'));

insert into tt_contract_location (id_contract, id_location) values ((select id
                                                                     from contract
                                                                     where contract.details = 'четвертый_заказ'),
                                                                    (select tt_organization_location.id_location
                                                                     from tt_organization_location
                                                                     where id_organization =
                                                                           (select contract.id_organization
                                                                            from contract
                                                                            where details = 'четвертый_заказ')));


insert into payment (id_contract, type, date, amount) VALUES ((select id
                                                               from contract
                                                               where details = 'четвертый_заказ'), 'телефонный заказ',
                                                              '2018-04-20',
                                                              200);

insert into shipment (delivery_date, planned_delivery_date, departure_date, planned_departure_date, id_contract)
values               ('2018-05-11',         '2018-05-11',     '2018-05-02',       '2018-05-02', (select id from contract where details = 'четвертый_заказ'))
returning id;

insert into tt_product_shipment (id_product, id_shipment) values ((select id
                                                                   from product
                                                                   where product.name = 'усилитель'), (select id
                                                                                                      from shipment
                                                                                                      where
                                                                                                        delivery_date =
                                                                                                        '2018-05-11' and
                                                                                                        departure_date =
                                                                                                        '2018-05-02'));





