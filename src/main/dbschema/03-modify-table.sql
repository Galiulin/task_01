ALTER TABLE public.shipment ADD id_contract int NULL;
COMMENT ON COLUMN public.shipment.id_contract IS 'договор по которому была осуществлена доставка';
ALTER TABLE public.shipment
ADD CONSTRAINT shipment_contract_id_fk
FOREIGN KEY (id_contract) REFERENCES contract (id);

update shipment set id_contract = (select id from contract where details = 'первый заказ')
where departure_date = '2018-05-20' and delivery_date = '2018-05-20';

update shipment set id_contract = (select id from contract where details = 'второй заказ')
where departure_date = '2018-04-23' and delivery_date = '2018-04-25';